package nl.avans.planningpoker.dialogues;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import nl.avans.planningpoker.R;
import nl.avans.planningpoker.activities.ExtraInformationActivity;
import nl.avans.planningpoker.adapter.ColorPickerAdapter;
import nl.avans.planningpoker.models.AsyncResponse;
import nl.avans.planningpoker.models.TaskGET;

/**
 * Created by Sander on 14-3-2015.
 */
public class ColorPickerDialog extends Dialog {

    private ColorPickerDialog _dialog;
    private ExtraInformationActivity aChooseColor;
    private String urlColors;
    private String urlParticipants;
    private int color;
    private ColorPickerAdapter adapter;
    private List<Integer> colorList = null;


    public ColorPickerDialog(Context context, String urlColors, String urlParticipants) {
        super(context);
        this._dialog = this;
        this.setTitle("Choose a color");
        this.urlColors = urlColors;
        this.urlParticipants = urlParticipants;
        aChooseColor = (ExtraInformationActivity)context;
        adapter = new ColorPickerAdapter(getContext());
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.color_picker);

        TaskGET tColors = new TaskGET(new AsyncResponse(){
            @Override
            public void processFinish(String output) {
                GridView gridViewColors = (GridView) findViewById(R.id.gridViewColors);
                Button btnClose = (Button) findViewById(R.id.color_picker_close);
                String colors[][] = {};

                if(!output.equals("[]")){
                    try {

                        if(output == null){
                            Toast.makeText(getContext().getApplicationContext(), "Could not fetch remote color set. Using local one instead.", Toast.LENGTH_SHORT).show();
                        }
                        JSONArray outputColors = null;
                        outputColors = new JSONArray(output);
                        final JSONArray t_outputColors = outputColors;

                        colorList = new ArrayList<Integer>() {{
                            for(int i =0; i<t_outputColors.length(); i++) {
                                JSONObject jsonObject = null;
                                try {
                                    jsonObject = t_outputColors.getJSONObject(i);
                                    add(Color.parseColor(((jsonObject.getString("hex_color_code").startsWith("#")) ? "" : "#") + jsonObject.getString("hex_color_code")));
                                } catch (JSONException e) {
                                    Log.e("Android", e.getMessage(), e);
                                }
                            }
                        }};
                       //adapter.setColorList(_dialog.colorList);
                    } catch (JSONException e) {
                        Log.e("Android", e.getMessage(), e);
                    }
                }
                final List<Integer> remoteColorSet = colorList;
                TaskGET tParticipants = new TaskGET(new AsyncResponse() {
                    @Override
                    public void processFinish(String output) {
                        JSONArray colorsOccupied = null;
                        try {
                            colorsOccupied = new JSONArray(output);
                            final JSONArray t_colorsOccupied = colorsOccupied;



                            List<Integer> clOccupied = new ArrayList<Integer>() {{
                                for(int i =0; i<t_colorsOccupied.length(); i++) {
                                    JSONObject jsonObject = null;
                                    try {
                                        jsonObject = t_colorsOccupied.getJSONObject(i);
                                        add(Color.parseColor(((jsonObject.getString("color").startsWith("#")) ? "" : "#") + jsonObject.getString("color")));
                                    } catch (JSONException e) {
                                        Log.e("Android", e.getMessage(), e);
                                    }
                                }
                            }};
                            setAdapterColorList(clOccupied);
                        } catch (JSONException e) {
                            Log.e("Android", e.getMessage(), e);
                        }
                    }
                });
                tParticipants.execute(urlParticipants);

                gridViewColors.setAdapter(adapter);

                // close the dialog on item click
                gridViewColors.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        ColorPickerDialog.this.dismiss();

                        color = adapter.getColor(position);

                        Button btnColor = (Button) aChooseColor.findViewById(R.id.btn_color);
                        btnColor.setBackgroundColor(color);
                    }
                });

                btnClose.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        _dialog.dismiss();
                    }
                });
            }
        });
        tColors.execute(urlColors);
    }

    private void setAdapterColorList(List<Integer> clOccupied) {
        adapter.setColorList(filterColorSet(_dialog.colorList, clOccupied));
        adapter.notifyDataSetChanged();
    }

    public int getColor() {
        return color;
    }

    public ColorPickerAdapter getAdapter() {
        return adapter;
    }

    private List<Integer> filterColorSet(List<Integer> colorSet, List<Integer> colorsOccupied){
        if (colorsOccupied != null) {
            colorSet.removeAll(colorsOccupied);
        }
        return colorSet;
    }
}
