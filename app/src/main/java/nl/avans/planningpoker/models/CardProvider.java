package nl.avans.planningpoker.models;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;

import nl.avans.planningpoker.R;
import nl.avans.planningpoker.activities.CardSelectionActivity;
import nl.avans.planningpoker.adapter.CardsListAdapter;

/**
 * Created by Sander on 11-6-2015.
 */
public class CardProvider {

    private ArrayList<Card> lCards;
    private CardProvider _provider;
    private CardSelectionActivity _aCardSelection;

    public CardProvider(CardSelectionActivity activity){
        _provider = this;
        this._aCardSelection = activity;
        this.lCards = new ArrayList<Card>();
        initCards();
    }

    public void initCards(){
        TaskGET tCards = new TaskGET(new AsyncResponse(){
            @Override
            public void processFinish(String response)
            {
                try {
                    JSONArray aCards = new JSONArray(response);
                    for(int i = 0, count = aCards.length(); i<count; i++) {
                        JSONObject oCard = aCards.getJSONObject(i);

                        Card card = new Card();
                        card.setName(oCard.getString("value"));
                        card.setPosition(oCard.optInt("position"));
                        card.setIcon(R.drawable.logo);

                        _provider.lCards.add(card);
                        _aCardSelection.addToCards(card);
                    }
                } catch (JSONException e) {
                    Log.e("Android", e.getMessage(), e);

                }
                Collections.sort(_provider.lCards, Card.DESCENDING_COMPARATOR);
                _aCardSelection.getList().setAdapter(new CardsListAdapter(_aCardSelection, _provider.lCards));

            }
        });
        tCards.execute("http://p-poker.herokuapp.com/api/fibinacci_values");
    }

    private class CustomComparator implements java.util.Comparator<Card> {
        @Override
        public int compare(Card lhs, Card rhs) {
            return (Integer.valueOf(((Card)lhs).getPosition()).compareTo(Integer.valueOf(((Card) rhs).getPosition())));
        }
    }
}
