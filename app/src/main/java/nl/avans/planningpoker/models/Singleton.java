package nl.avans.planningpoker.models;

import android.app.Activity;
import android.content.SharedPreferences;
import android.util.Log;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;

/**
 * Created by Louis Hol on 25-5-2015.
 */
public class Singleton {

    private static Singleton mInstance = null;
    public static Socket mSocket = null;
    private static Activity act;

    public static Singleton getInstance()
    {
        if(mInstance == null)
        {
            mInstance = new Singleton();
        } else {
            Log.d("myapp", "connectie is al tot stand");
        }
        return mInstance;
    }
    public static Singleton getInstance(Activity t){
        if(mInstance == null)
        {
            mInstance = new Singleton();
            act = t;
            initSingleton();
        } else {
            Log.d("myapp", "connectie is al tot stand");
        }
        return mInstance;
    }
    private static void initSingleton()
    {
        try {
            mSocket = IO.socket("http://p-poker.herokuapp.com/");
            mSocket.on("whois", whois);
            Log.d("myapp", "connectie tot stand ");
            mSocket.connect();
        } catch (URISyntaxException e) {
            Log.e("Android", e.getMessage(), e);
        }
    }
    private static Emitter.Listener whois = new Emitter.Listener() {

        @Override
        public void call(final Object... args) {

            SharedPreferences settings = act.getSharedPreferences("SessionPreferences", 0);
            final String sessionID = settings.getString("sessionID", null);
            Log.d("settings", settings.toString());
            final String participant_name = settings.getString("participant_name", null);
            final String participant_color = settings.getString("participant_color", null);
            act.runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    try {
                            JSONObject json = new JSONObject();
                            json.put("participant_name", participant_name);
                            json.put("participant_color", participant_color);
                            json.put("room_id", sessionID);

                            Log.d("iam", json.toString());
                            mSocket.emit("iam", json.toString());
                        } catch (JSONException e) {
                            Log.e("Android", e.getMessage(), e);
                        }
                }
            });
        }
    };
}
