package nl.avans.planningpoker.models;

import java.io.Serializable;
import java.util.Comparator;

/**
 * Created by Sander on 14-3-2015.
 */
public class Card implements Serializable, Comparable<Card> {
    private String name;
    private int position;
    private int rIcon;

    public static final Comparator<Card> DESCENDING_COMPARATOR = new Comparator<Card>() {
        // Overriding the compare method to sort the age
        public int compare(Card c1, Card c2) {
            return c1.position - c2.position;
        }
    };

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setIcon(int icon) {
        this.rIcon = icon;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public int getPosition() {
        return position;
    }

    @Override
    public int compareTo(Card c) {
        return (this.name).compareTo(c.name);
    }
}
