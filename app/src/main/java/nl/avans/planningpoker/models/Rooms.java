package nl.avans.planningpoker.models;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by Sander on 6-4-2015.
 */
public class Rooms {

    private JSONObject obj;
    private List<String> participants;
    public Rooms(String jsonstring)
    {
        parseJson(jsonstring);
    }
    private void parseJson(String jsonstring)
    {
        try
        {
            obj = new JSONObject(jsonstring);
        }
        catch(JSONException e)
        {
            e.printStackTrace();
            Log.e("Android", e.getMessage(), e);
        }
    }

    public JSONObject getRoomByIndex(Integer index)
    {
        JSONObject room = null;
        try
        {
            if(obj.length() > 0)
            {
                if(obj.getJSONObject("Rooms") != null)
                    room = obj.getJSONObject("Rooms").getJSONObject(index.toString());
            }
        }
        catch(JSONException e)
        {
            Log.e("Android", e.getMessage(), e);
        }
        return room;
    }

    public Integer getID(JSONObject obj) {
        Integer id = null;
        try
        {
            id =  obj.getInt("id");
        }
        catch(JSONException e)
        {
            Log.e("Android", e.getMessage(), e);
        }
        return id;
    }

    public List<String> getParticipants(JSONObject obj)
    {
        participants = null;
        try
        {
            participants.add(obj.getJSONObject("results").getString("name"));
        }
        catch(JSONException e)
        {
            Log.e("Android", e.getMessage(), e);
        }
        return participants;
    }
}