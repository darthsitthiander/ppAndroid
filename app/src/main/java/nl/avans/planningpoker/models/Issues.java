package nl.avans.planningpoker.models;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Louis on 6-4-2015.
 */
public class Issues {

    private JSONObject obj;
    public Issues(String jsonstring)
    {
       parseJson(jsonstring);
    }
    private void parseJson(String jsonstring)
    {
        try
        {
            obj = new JSONObject(jsonstring);
        }
        catch(JSONException e)
        {
            Log.e("Android", e.getMessage(), e);
        }
    }

    public JSONObject getIssueByIndex(int index)
    {
        JSONObject fields = null;
        try
        {
            int length = obj.getJSONArray("issues").length();
            if(index < length)
            {
                fields = obj.getJSONArray("issues").getJSONObject(index);
            }
        }
        catch(JSONException e)
        {
            Log.e("Android", e.getMessage(), e);
        }
        return fields;
    }

    public String getDescription(JSONObject obj , int index)
    {
        String description = "";
        try
        {
            if(obj != null)
            {
                description =  "Key " + obj.getString("key") + "| Beschrijving van index " + index++;
            }
        }
        catch(JSONException e)
        {
            description = "Geen beschrijving mogelijk";
            Log.e("Android", e.getMessage(), e);

        }
        return description;
    }
}

