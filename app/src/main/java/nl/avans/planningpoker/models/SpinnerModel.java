package nl.avans.planningpoker.models;

/**
 * Created by Sander on 19-5-2015.
 */
public class SpinnerModel {

    private  String Name="";
    private  String Color="";
    private  String id = "";

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getID() {
        return id;
    }

    public void setID (String _id) {
        id = _id;
    }

    public String getColor() {
        return Color;
    }

    public void setColor(String color) {
        Color = color;
    }
}