package nl.avans.planningpoker.models;

import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by Jarno on 5-4-2015.
 */

public class TaskGET extends AsyncTask<String, String, String>
{
    public AsyncResponse delegate=null;
    private HashMap<String, String> headers;

    public TaskGET(AsyncResponse response)
    {
        delegate = response;
        headers = new HashMap<>();
    }

    @Override
    protected void onPostExecute(String result) {
        delegate.processFinish(result);
    }
    @Override
    public String doInBackground(String... urls) {
        try {
            String url = urls[0];
            HttpGet httpget = new HttpGet(url);

            if (this.headers.size() > 0)
            {
                Iterator it = this.headers.entrySet().iterator();

                while (it.hasNext())
                {
                    Map.Entry pair = (Map.Entry) it.next();

                    httpget.addHeader((String) pair.getKey(), (String) pair.getValue());
                    it.remove();
                }
            }

            HttpClient httpclient = new DefaultHttpClient();
            HttpResponse response = httpclient.execute(httpget);
            // StatusLine stat = response.getStatusLine();
            int status = response.getStatusLine().getStatusCode();
            System.out.println("Status is " + status);
            if (status == 200) {
                HttpEntity entity = response.getEntity();
                String data = EntityUtils.toString(entity);
                return data;
            }
        } catch (IOException e) {
            Log.e("Android", e.getMessage(), e);
        }
        return "[]";
    }

    public void addHeader (String header, String content)
    {
        this.headers.put(header, content);
    }

    public void clearHeaders ()
    {
        this.headers.clear();
    }
}
