package nl.avans.planningpoker.models;

/**
 * Created by Jarno on 5-4-2015.
 */
public interface AsyncResponse
{
    void processFinish(String output);
}
