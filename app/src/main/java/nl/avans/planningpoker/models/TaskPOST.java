package nl.avans.planningpoker.models;

import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Sander on 10-5-2015.
 */
public class TaskPOST extends AsyncTask<Map<String, String>, String, String>
{
    private AsyncResponse delegate=null;

    public TaskPOST(AsyncResponse response)
    {
        delegate = response;
    }

    @Override
    protected String doInBackground(Map<String, String>... data) {
        try {

            String url = data[0].get("url");
            HttpPost httppost = new HttpPost(url);
            HttpClient httpclient = new DefaultHttpClient();

            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);

            for(Map.Entry<String,String> entry : data[0].entrySet()) {
                String key = entry.getKey();
                String value = entry.getValue();

                if(!key.equals("url")) {
                    nameValuePairs.add(new BasicNameValuePair(key, value));
                }

            }
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

            HttpResponse response = httpclient.execute(httppost);
            // StatusLine stat = response.getStatusLine();
            int status = response.getStatusLine().getStatusCode();
            System.out.println("Status is " + status);
            if (status == 200) {
                HttpEntity entity = response.getEntity();
                String responseData = EntityUtils.toString(entity);
                return responseData;
            }
        } catch (IOException e) {
            Log.e("Android", e.getMessage(), e);
        }
        return "[]";
    }

    @Override
    protected void onPostExecute(String result) {
        delegate.processFinish(result);
    }
}
