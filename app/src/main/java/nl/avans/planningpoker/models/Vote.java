package nl.avans.planningpoker.models;

        import java.io.Serializable;

/**
 * Created by Sander on 14-3-2015.
 */
public class Vote implements Serializable {
    private String user;
    private String vote;
    private String color;

    public void setUser(String user){
        this.user = user;
    }
    public String getUser(){
        return this.user;
    }

    public String getColor() {return this.color;}
    public void setColor(String _color){this.color = _color;}

    public void setVote(String vote){
        this.vote = vote;
    }
    public String getVote(){
        return this.vote;
    }
}