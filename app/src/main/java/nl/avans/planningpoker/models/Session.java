package nl.avans.planningpoker.models;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.Serializable;

/**
 * Created by Sander on 13-4-2015.
 */
public class Session implements Serializable {

    private JSONArray obj;
    private String feed;
    public Session(String jsonstring)
    {
        this.feed = jsonstring;
        parseJson(jsonstring);
    }
    private void parseJson(String jsonstring)
    {
        try
        {
            obj = new JSONArray(jsonstring);
        }
        catch(JSONException e)
        {
            Log.e("Android", e.getMessage(), e);
            e.printStackTrace();
        }
    }

    public int getKey(int index){
        int key = -1;
        try {
            key = obj.getJSONObject(index).getInt("key");
        } catch (JSONException e) {
            Log.e("Android", e.getMessage(), e);
            e.printStackTrace();
        }
        return key;
    }

    public String getJSON(){
        return this.feed;
    }
}
