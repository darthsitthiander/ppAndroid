package nl.avans.planningpoker.models;

/**
 * Created by Sander on 7-4-2015.
 */
public class Participant {

    private String name;
    private String id;

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
}