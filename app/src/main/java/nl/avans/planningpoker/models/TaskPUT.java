package nl.avans.planningpoker.models;

import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by Sander on 10-5-2015.
 */
public class TaskPUT extends AsyncTask<Map<String, String>, String, String>
{
    private AsyncResponse delegate=null;
    private HashMap<String, String> headers;

    public TaskPUT(AsyncResponse response)
    {
        delegate = response;
        headers = new HashMap<>();
    }

    @Override
    protected String doInBackground(Map<String, String>... data) {
        try {

            String url = data[0].get("url");
            HttpPut putRequest = new HttpPut(url);

            if (this.headers.size() > 0)
            {
                Iterator it = this.headers.entrySet().iterator();

                while (it.hasNext())
                {
                    Map.Entry pair = (Map.Entry) it.next();

                    putRequest.addHeader((String) pair.getKey(), (String) pair.getValue());
                    it.remove();
                }
            }

            HttpClient httpclient = new DefaultHttpClient();

            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

            boolean entitySet = false;

            for(Map.Entry<String,String> entry : data[0].entrySet()) {
                String key = entry.getKey();
                String value = entry.getValue();

                if(key.equals("json")) {
                    putRequest.setEntity(new StringEntity(value));
                    entitySet = true;
                }
                else
                if(!key.equals("url")) {
                    nameValuePairs.add(new BasicNameValuePair(key, value));
                }

            }
            if (!entitySet) {
                putRequest.setEntity(new UrlEncodedFormEntity(nameValuePairs, HTTP.UTF_8));
            }
            HttpResponse response = httpclient.execute(putRequest);
            // StatusLine stat = response.getStatusLine();
            int status = response.getStatusLine().getStatusCode();
            System.out.println("Status is " + status);
            if (status == 200) {
                HttpEntity entity = response.getEntity();
                String responseData = EntityUtils.toString(entity);
                return responseData;
            }
        } catch (IOException e) {
            Log.e("Android", e.getMessage(), e);
        }
        return "[]";
    }

    @Override
    protected void onPostExecute(String result) {
        delegate.processFinish(result);
    }

    public void addHeader (String header, String content)
    {
        this.headers.put(header, content);
    }

    public void clearHeaders ()
    {
        this.headers.clear();
    }
}
