package nl.avans.planningpoker.fragments;

import android.os.Bundle;
import android.preference.PreferenceFragment;

import nl.avans.planningpoker.R;

/**
 * Created by Sander on 23-5-2015.
 */
public class SettingsFragment extends PreferenceFragment {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.prefs);
    }
}
