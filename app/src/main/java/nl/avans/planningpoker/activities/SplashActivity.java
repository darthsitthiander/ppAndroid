package nl.avans.planningpoker.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.Toast;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import nl.avans.planningpoker.R;
import nl.avans.planningpoker.models.AsyncResponse;
import nl.avans.planningpoker.models.TaskGET;

/**
 * Created by Sander on 19-5-2015.
 */
public class SplashActivity extends Activity {
    /** Duration of wait **/
    private final int SPLASH_DISPLAY_LENGTH = 1000; // minimum delay (ensures the logo a certain amount of time to be shown)
    private String json;
    private static final ScheduledExecutorService worker = Executors.newSingleThreadScheduledExecutor();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);


        someMethod();

        TaskGET tJSON = new TaskGET(new AsyncResponse(){
            @Override
            public void processFinish(String output) {
                json = output;

                int secondsDelayed = 1;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(SplashActivity.this, JoinRoomActivity.class);
                        intent.putExtra("json", json);

                        startActivity(intent);
                        finish();
                    }
                }, secondsDelayed * SPLASH_DISPLAY_LENGTH);
            }
        });
        tJSON.execute("http://p-poker.herokuapp.com/api/sessions/");
    }

    void someMethod() {
        Runnable task = new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getApplicationContext(), "There seems to be a problem reaching the host.", Toast.LENGTH_SHORT).show();
            }
        };
        worker.schedule(task, 5, TimeUnit.SECONDS);
    }
}
