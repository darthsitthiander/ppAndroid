package nl.avans.planningpoker.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import nl.avans.planningpoker.R;
import nl.avans.planningpoker.models.AsyncResponse;
import nl.avans.planningpoker.models.Card;
import nl.avans.planningpoker.models.CardProvider;
import nl.avans.planningpoker.models.Singleton;
import nl.avans.planningpoker.models.TaskPOST;


public class CardSelectionActivity extends Activity {

    private Socket mSocket = Singleton.getInstance().mSocket;

    private List<Card> cards;
    private String jsonString;
    private JSONObject jsonObject;
    private String issueid;
    private int pos;
    private String userid;
    private ListView list;
    private Dialog coffeeDialog;
    private boolean bChoseBreak = false;
    private CardSelectionActivity _activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this._activity = this;
        setContentView(R.layout.activity_card_selection);

        mSocket.on("break", onBreak);
        mSocket.on("continue", onContinue);

        getJson();
        setDescriptionTitle();

        CardProvider cardProvider = new CardProvider(this);

        cards = new ArrayList<Card>();

        list = (ListView) findViewById(R.id.l_cards);
        list.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> arg0, View view, int position, long id) {
                        pos = position;
                    }
                }
        );
    }
    @Override
    public void onBackPressed() {
        //Ask the user if they want to quit
        final CardSelectionActivity activity = this;
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle(R.string.quit)
                .setMessage(R.string.really_quit)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        startActivity(new Intent(activity, JoinRoomActivity.class));
                        //Stop the activity
                        CardSelectionActivity.this.finish();
                    }

                })
                .setNegativeButton(R.string.no, null)
                .show();
    }

    public void showText(View view)
    {
        TextView t = (TextView)findViewById(R.id.txt_task_title);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(t.getText())
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        Log.d("Android", "ok");
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }
    public void pickCard(View view)
    {

        SharedPreferences settings = getSharedPreferences("SessionPreferences", 0);

        Toast toast = Toast.makeText(getApplicationContext(), "One moment please...", Toast.LENGTH_SHORT);
        toast.show();
        String vote = cards.get(pos).getName();
        try
        {
            issueid = jsonObject.getString("id");

            String jsonData = "";
            //jObj = new JSONObject(json.substring(3));
            jsonData = settings.getString("user", null);
            userid = jsonData;
            Log.d("settings.getstring user", String.valueOf(userid));
            //userid = new JSONObject(settings.getString("user", null)).getString("id");
        }
        catch (JSONException e)
        {
            Log.e("Android", e.getMessage(), e);
        }
        HashMap<String, String> data = new HashMap<String, String>();
        data.put("url", "http://p-poker.herokuapp.com/api/Issues/"+ issueid  + "/ratings");
        data.put("userid", userid);
        data.put("rating", vote);

        TaskPOST tp = new TaskPOST(new AsyncResponse(){
            @Override
            public void processFinish(String response)
            {

                try {
                    SharedPreferences settings = getSharedPreferences("SessionPreferences", 0);
                    String participant_name = settings.getString("participant_name", null);
                    String participant_color = settings.getString("participant_color", null);
                    JSONObject voteObject = new JSONObject("{vote: '" + cards.get(pos).getName() + "', participant_name: '" + participant_name + "', participant_color: '" + participant_color + "'}");
                    Log.d("vote", String.valueOf(voteObject));

                    Intent intent = new Intent(getApplicationContext(), WaitingScreenActivity.class);
                    String card_value = cards.get(pos).getName();


                    if("coffee".equalsIgnoreCase(card_value)){
                        _activity.bChoseBreak = true;
                        Log.d("_activity.bChoseBreak", String.valueOf(_activity.bChoseBreak));
                        mSocket.emit("vote", voteObject);
                        AlertDialog.Builder builder = new AlertDialog.Builder(CardSelectionActivity.this);
                        builder.setMessage("Press 'Continue' to carry on with the session")
                                .setTitle("Break")
                                .setPositiveButton("Continue", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        mSocket.emit("continue");
                                    }
                                });
                        AlertDialog dialog = builder.create();
                        dialog.show();
                    } else {
                        _activity.bChoseBreak = false;
                        Log.d("_activity.bChoseBreak", String.valueOf(_activity.bChoseBreak));
                        Log.d("position: ", String.format("value = %d", pos));
                        intent.putExtra("card_value", card_value);
                        intent.putExtra("json" , jsonString);
                        intent.putExtra("vote" , String.valueOf(voteObject));
                        startActivity(intent);
                    }
                } catch (JSONException e) {
                    Log.e("Android", e.getMessage(), e);
                }
            }
        });
        tp.execute(data);
    }
    private void getJson()
    {
        try {
            jsonString = getIntent().getStringExtra("json");
            jsonObject =  new JSONObject(jsonString);
        } catch (Exception e) {
            jsonObject = null;
            Log.e("Android", e.getMessage(), e);
        }
        setDescriptionTitle();
    }

    private void setDescriptionTitle()
    {
        if(jsonObject != null)
        {
            TextView descrip = (TextView)findViewById(R.id.txt_task_title);
            try {
                String description = jsonObject.getString("description");
                descrip.setText(description);
            } catch (JSONException e) {

                Log.e("Android", e.getMessage(), e);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public ListView getList() {
        return this.list;
    }

    private Emitter.Listener onBreak = new Emitter.Listener() {

        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.d("BREAK", "");
                    if (!_activity.bChoseBreak) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(CardSelectionActivity.this);
                        builder.setMessage("Someone in your team really needs a break.");
                        ImageView image = new ImageView(getApplicationContext());
                        image.setImageResource(R.drawable.coffee);
                        image.setPadding(80, 40, 80, 40);
                        builder.setView(image);
                        coffeeDialog = builder.create();
                        coffeeDialog.setCancelable(false);
                        coffeeDialog.setCanceledOnTouchOutside(false);
                        coffeeDialog.show();
                    }
                    Log.d("_activity.bChoseBreak", String.valueOf(_activity.bChoseBreak));
                }
            });
        }
    };
    private Emitter.Listener onContinue = new Emitter.Listener() {

        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.d("CONTINUE", "");
                    if (!_activity.bChoseBreak) {
                        coffeeDialog.dismiss();
                    }
                    _activity.bChoseBreak = false;
                    Log.d("_activity.bChoseBreak", String.valueOf(_activity.bChoseBreak));
                }
            });
        }
    };

    public void addToCards(Card card){
        Log.d("ANdroid", "Card toegevoegt");
        this.cards.add(card);
    }
}