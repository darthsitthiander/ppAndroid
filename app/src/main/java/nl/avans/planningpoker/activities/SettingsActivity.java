package nl.avans.planningpoker.activities;

import android.os.Bundle;
import android.preference.PreferenceActivity;

import nl.avans.planningpoker.fragments.SettingsFragment;

/**
 * Created by Sander on 23-5-2015.
 */
public class SettingsActivity extends PreferenceActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Display the fragment as the main content.
        getFragmentManager().beginTransaction()
                .replace(android.R.id.content, new SettingsFragment())
                .commit();
    }
}
