package nl.avans.planningpoker.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormatSymbols;

import nl.avans.planningpoker.R;
import nl.avans.planningpoker.models.Singleton;


public class WaitingScreenActivity extends Activity {

    private Socket mSocket = Singleton.getInstance().mSocket;
    private Dialog coffeeDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_waiting_screen);

        coffeeDialog = new Dialog(this);
        AlertDialog.Builder builder = new AlertDialog.Builder(WaitingScreenActivity.this);
        builder.setMessage("Someone in your team really needs a break.");
        ImageView image = new ImageView(getApplicationContext());
        image.setImageResource(R.drawable.kaart);
        builder.setView(image);
        coffeeDialog = builder.create();
        coffeeDialog.setCancelable(false);
        coffeeDialog.setCanceledOnTouchOutside(false);

        JSONObject voteObject = null;
        try {
            voteObject = new JSONObject(getIntent().getStringExtra("vote"));
            Log.d("voteObject", voteObject.toString());
            mSocket.emit("vote", voteObject);
        } catch (JSONException e) {
            Log.e("Android", e.getMessage(), e);
        }
        mSocket.on("break", onBreak);
        mSocket.on("continue", onContinue);
        mSocket.on("voted", voted);
        mSocket.on("issue result", onIssueResult);
        Log.d("myuapp", "IS DE SOCKET GECONNECT?? " + mSocket.connected());
        // get data from previous screen
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String value = extras.getString("card_value");
            TextView txt_card = (TextView) findViewById(R.id.txt_ws_card);

            // Font Awesome
            Typeface font;
            font = Typeface.createFromAsset(getAssets(), "fontawesome-webfont.ttf");
            txt_card.setTypeface(font);

            String cardValue = "";
            switch(value.toLowerCase()){
                case "infinite": cardValue = DecimalFormatSymbols.getInstance().getInfinity();
                    txt_card.setTypeface(null, Typeface.NORMAL);
                    break; //R.string.fa_spinner //"?"
                case "unsure": cardValue = "?"; break;
                default:cardValue = value;
                    txt_card.setTypeface(null, Typeface.NORMAL);
            }
            txt_card.setText(cardValue);

        }
        WebView web = (WebView) findViewById(R.id.webView);
        web.setBackgroundColor(Color.TRANSPARENT); //for gif without background
        web.loadUrl("file:///android_asset/loadingindicator.html");
    }

    private void goToResults(String data)
    {

        Log.d("myapp", "Data is " + data);
        Intent mainIntent = new Intent(WaitingScreenActivity.this, ResultsActivity.class);
        String json = getIntent().getStringExtra("json");
        mainIntent.putExtra("json",json);
        mainIntent.putExtra("vote", data);
        mainIntent.putExtra("card_value", getIntent().getStringExtra("card_value"));
        startActivity(mainIntent);
    }

    private Emitter.Listener onIssueResult = new Emitter.Listener() {

        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.d("Issueresults", "Ze zijn binnen!");
                    goToResults(args[0].toString());
                }
            });
        }
    };
    private Emitter.Listener voted = new Emitter.Listener() {

        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.d("voted", "Event binnen");
                    parseJSONVotes(args[0].toString());
                }
            });
        }
    };

    private void parseJSONVotes(String json)
    {
        Log.d("JSONVOTED", json);
        TextView footer = (TextView)findViewById(R.id.textView3);
        try {
            JSONObject obj = new JSONObject(json);
            int voted = obj.getInt("voted");
            footer.setText(voted + " participant" + ((voted==1)?" has":"s have") + " cast a vote.");
        } catch (Exception e) {
            Log.e("Android", e.getMessage(), e);
        }
    }

    @Override
    public void onBackPressed() {
        //Ask the user if they want to quit
        final WaitingScreenActivity _activity = this;
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle(R.string.quit)
                .setMessage(R.string.really_quit)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        startActivity(new Intent(_activity, JoinRoomActivity.class));
                        //Stop the activity
                        WaitingScreenActivity.this.finish();
                    }

                })
                .setNegativeButton(R.string.no, null)
                .show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        mSocket.off("issue result", onIssueResult);
        mSocket.off("voted", voted);
    }

    private Emitter.Listener onBreak = new Emitter.Listener() {

        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.d("BREAK", "");
                    coffeeDialog.show();
                }
            });
        }
    };
    private Emitter.Listener onContinue = new Emitter.Listener() {

        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.d("CONTINUE", "");
                    coffeeDialog.dismiss();
                }
            });
        }
    };

}