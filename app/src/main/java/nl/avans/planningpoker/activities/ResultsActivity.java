package nl.avans.planningpoker.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.List;

import nl.avans.planningpoker.R;
import nl.avans.planningpoker.adapter.VotesListAdapter;
import nl.avans.planningpoker.models.Singleton;
import nl.avans.planningpoker.models.Vote;


public class ResultsActivity extends Activity {

    private Socket mSocket = Singleton.getInstance().mSocket;

    String json;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSocket.on("new issue", onNewIssue);
        mSocket.on("finished", onFinished);
        setContentView(R.layout.activity_results);

        setFields();
    }

    private Emitter.Listener onFinished = new Emitter.Listener() {

        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    String text = "Het spelen van de sessie is afgelopen";
                    Toast toast = Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT);
                    toast.show();
                }
            });
        }
    };
    private Emitter.Listener onNewIssue = new Emitter.Listener() {

        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    goToIssue(data.toString());
                }
            });
        }
    };
    private void goToIssue(String data)
    {
        Intent intent = new Intent(this, CardSelectionActivity.class);
        intent.putExtra("json", data);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        //Ask the user if they want to quit
        final ResultsActivity _activity = this;
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle(R.string.quit)
                .setMessage(R.string.really_quit)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        startActivity(new Intent(_activity, JoinRoomActivity.class));
                        //Stop the activity
                        ResultsActivity.this.finish();
                    }

                })
                .setNegativeButton(R.string.no, null)
                .show();
    }

    public void showCard(View view)
    {
        Intent intent = new Intent(this, ShowCardActivity.class);
        intent.putExtra("card_value", getIntent().getStringExtra("card_value"));
        intent.putExtra("vote", getIntent().getStringExtra("vote"));
        startActivity(intent);
    }
    private void setFields()
    {
        List<Vote> votes = new ArrayList<Vote>();
        try {
            JSONObject obj = new JSONObject(getIntent().getStringExtra("vote"));
            JSONArray _votes = obj.getJSONArray("votes");
            for (int i = 0; i < _votes.length(); i++) {
                JSONObject _vote = _votes.getJSONObject(i);
                Vote item = new Vote();
                item.setVote(_vote.getString("vote"));
                item.setColor(_vote.getString("participant_color"));
                item.setUser(_vote.getString("participant_name"));
                votes.add(item);
            }
            TextView txtMode = (TextView) findViewById(R.id.modus);
            JSONObject _modeObject = (JSONObject) obj.getJSONArray("modus").get(0);
            int countMode = _modeObject.getInt("count");
            JSONArray _modeValues = _modeObject.getJSONArray("values");
            String valuesString = "";
            for (int i = 0; i < _modeValues.length(); i++) {
                JSONObject _modeValue = _modeValues.getJSONObject(i);
                String voteString = _modeValue.getString("value");

                if(voteString.equalsIgnoreCase("unsure")){
                    voteString = "?";
                } else if (voteString.equalsIgnoreCase("infinite")){
                    voteString = DecimalFormatSymbols.getInstance().getInfinity();
                }
                valuesString += voteString.toString();
                if(i<_modeValues.length()-1){
                    valuesString += ", ";
                }

            }
            txtMode.setText("Mode: " + valuesString + " (" + String.valueOf(countMode) + ")");
        } catch(JSONException e) {
            Log.e("Android", e.getMessage(), e);
        }

        VotesListAdapter itemArrayAdapter = new VotesListAdapter(this, votes);

        ListView list = (ListView) findViewById(R.id.l_results);
        TextView txtResultsHeader = (TextView)findViewById(R.id.textView3);
        txtResultsHeader.setTypeface(null, Typeface.NORMAL);

        list.setAdapter(itemArrayAdapter);
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mSocket.disconnect();
        mSocket.off("new issue", onNewIssue);
    }
}
