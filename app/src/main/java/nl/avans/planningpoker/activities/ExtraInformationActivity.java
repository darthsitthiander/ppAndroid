package nl.avans.planningpoker.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import nl.avans.planningpoker.R;
import nl.avans.planningpoker.dialogues.ColorPickerDialog;
import nl.avans.planningpoker.models.AsyncResponse;
import nl.avans.planningpoker.models.SpinnerModel;
import nl.avans.planningpoker.models.TaskGET;
import nl.avans.planningpoker.models.TaskPOST;
import nl.avans.planningpoker.models.TaskPUT;


public class ExtraInformationActivity extends Activity {

    private ExtraInformationActivity _activity = null;
    public static final  List<SpinnerModel> SpinnerValues = new ArrayList<>();
    JSONArray participants;
    private String sessionID;
    ColorPickerDialog dialog;
    private int color;
    Button btn_Color;
    TextView txt_Name;
    private Map<String, HashMap<String, String>> entries;
    private Spinner s_User;
    private RadioButton r_newUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_extra_information);
        SharedPreferences settings = getSharedPreferences("SessionPreferences", 0);

        _activity  = this;

        sessionID = settings.getString("sessionID", "");


        getParticipants(sessionID);

        dialog = new ColorPickerDialog(ExtraInformationActivity.this, "http://p-poker.herokuapp.com/api/colors", "http://p-poker.herokuapp.com/api/sessions/" + sessionID + "/participants");
        btn_Color = (Button) findViewById(R.id.btn_color);
        txt_Name = (TextView) findViewById(R.id.txt_screen_name);
        s_User = (Spinner) findViewById(R.id.sp_user);
        s_User.setEnabled(false);
        r_newUser = (RadioButton) findViewById(R.id.r_new_user);

        setRadioListener();

        TaskGET tColors = new TaskGET(new AsyncResponse(){
            @Override
            public void processFinish(String response)
            {
                List<Integer> lColors = new ArrayList<Integer>() {{
                    add(1);
                    add(2);
                    add(3);
                }};
                //Hier stond dit dialog getAdapter() setColorList (lColors);
            }
        });
        tColors.execute("http://p-poker.herokuapp.com/api/colors");
    }

    private void setRadioListener() {
        RadioGroup rg = (RadioGroup) findViewById(R.id.rg_choose_user);
        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.r_choose_user:
                        btn_Color.setEnabled(false);
                        txt_Name.setEnabled(false);

                        s_User.setEnabled(true);
                        break;
                    case R.id.r_new_user:
                        btn_Color.setEnabled(true);
                        txt_Name.setEnabled(true);
                        s_User.setEnabled(false);
                        break;
                    default:
                        break;
                }
            }
        });
    }

    private void getParticipants(String sessionID)
    {
        TaskGET tJSON = new TaskGET(new AsyncResponse(){
            @Override
            public void processFinish(String feedParticipants)
            {
                try {
                    _activity.participants =  new JSONArray(feedParticipants);

                    JSONArray jsonArray = _activity.participants;
                    for(int i = 0, count = jsonArray.length(); i< count; i++)
                    {
                        final SpinnerModel sched = new SpinnerModel();

                        JSONObject jsonObject = jsonArray.getJSONObject(i);

                        /******* Firstly take data in model object ******/
                        sched.setColor(jsonObject.getString("color"));
                        sched.setName(jsonObject.getString("name"));
                        sched.setID(jsonObject.getString("id"));
                        /******** Take Model Object in ArrayList **********/
                        SpinnerValues.add(sched);
                    }

                    s_User.setAdapter(new CustomSpinnerAdapter(ExtraInformationActivity.this, R.layout.custom_spinner, SpinnerValues));

                } catch (JSONException e) {
                    Log.e("Android", e.getMessage(), e);
                }
            }
        });
        tJSON.execute("http://p-poker.herokuapp.com/api/sessions/" + sessionID + "/participants");
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent i = new Intent(this, SettingsActivity.class);
            startActivity(i);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onClickBtnColorPicker(View view){
        dialog.show();
    }

    public List<String> getOccupiedColors(){
        getParticipants(this.sessionID);
        List<String> colors = new ArrayList<String>();

        for(int i =0; i<this.participants.length(); i++) {
            JSONObject jsonObject = null;
            try {
                jsonObject = this.participants.getJSONObject(i);
                if(jsonObject.has("color")){
                    String clr = jsonObject.getString("color");
                    colors.add(clr);
                }
            } catch (JSONException e) {
                Log.e("Android", e.getMessage(), e);
                e.printStackTrace();
            }
        }
        return colors;
    }

    public void addParticipant(String strTxtScreenName)
    {
        TaskPOST t = new TaskPOST(new AsyncResponse(){
            @Override
            public void processFinish(String response)
            {
                Intent intent = new Intent(ExtraInformationActivity.this, IssueWaitingScreen.class);
                Log.d("usr", response.toString());
                saveUserSettings(response);
                startActivity(intent);
            }
        });
        String address = "http://p-poker.herokuapp.com/api/sessions/" + sessionID + "/participants";

        Map<String, String> data = new HashMap<String, String>();
        data.put("url", address);
        data.put("name", strTxtScreenName);
        String hexColor = String.format("#%06X", 0xFFFFFF & getColor());
        data.put("color", String.valueOf(hexColor));

        t.execute(data);
    }

    public void onClickBtnSubmitChooseColor(View view){
        getEntries();

        String strTxtScreenName = txt_Name.getText().toString();

        if(r_newUser.isChecked()){
            // check color availability
            if(!colorAvailable()){
                Toast.makeText(getApplicationContext(), "This color is already in use. Please choose another.", Toast.LENGTH_SHORT).show();
                return;
            }

            if(!strTxtScreenName.equals("")){
                if(!entries.containsKey(strTxtScreenName)){
                    Toast.makeText(getApplicationContext(), "User will be added to current session.", Toast.LENGTH_SHORT).show();

                    // add user to participants
                    addParticipant(strTxtScreenName);


                } else {
                    Map<String, String> values = (HashMap<String, String>) entries.get(strTxtScreenName);
                    updateParticipantColor(values.get("id"));
                }
            }
        } else {

            SpinnerModel selected = (SpinnerModel) s_User.getAdapter().getItem(s_User.getSelectedItemPosition());
            SharedPreferences settings = getSharedPreferences("SessionPreferences", 0);
            SharedPreferences.Editor editor = settings.edit();
            Log.d("opslaan user",selected.getID());
            editor.putString("user", selected.getID());
            editor.putString("participant_name", selected.getName());
            editor.putString("participant_color", selected.getColor());
            // Commit the edits!
            editor.commit();
            Intent intent = new Intent(ExtraInformationActivity.this, IssueWaitingScreen.class);
            startActivity(intent);

        }


    }

    private boolean colorAvailable() {
        String hexColor = String.format("#%06X", 0xFFFFFF & getColor());
        return !(getOccupiedColors().contains(hexColor));
    }

    private void saveUserSettings(String user)
    {
        try {
            SharedPreferences settings = getSharedPreferences("SessionPreferences", 0);
            SharedPreferences.Editor editor = settings.edit();
            JSONObject obj = new JSONObject(user);
            editor.putString("user", obj.getString("id"));
            editor.putString("participant_name", obj.getString("name"));
            editor.putString("participant_color", obj.getString("color"));
            editor.commit();
        } catch (Exception e) {
            Log.e("Android", e.getMessage(), e);
        }

    }

    private void updateParticipantColor(String id) {
        TaskPUT t = new TaskPUT(new AsyncResponse()
        {
            @Override
            public void processFinish(String response)
            {
                Intent intent = new Intent(ExtraInformationActivity.this, IssueWaitingScreen.class);
                saveUserSettings(response);
                startActivity(intent);
            }
        });

        String address = "http://p-poker.herokuapp.com/api/sessions/" + sessionID + "/participants/" + id;

        Map<String, String> data = new HashMap<String, String>();
        data.put("url", address);
        String hexColor = String.format("#%06X", 0xFFFFFF & getColor());
        data.put("color", String.valueOf(hexColor));

        t.execute(data);
    }

    public int getColor(){
        this.color = ((ColorDrawable) ((Button) findViewById(R.id.btn_color)).getBackground()).getColor();
        return this.color;
    }

    public Map<String,HashMap<String,String>> getEntries() {
        List<String> names = new ArrayList<String>();
        List<String> colors = new ArrayList<String>();
        entries = new HashMap<String, HashMap<String, String>>();

        if(this.participants != null){
            for(int i =0; i<this.participants.length(); i++) {
                JSONObject jsonObject = null;
                try {
                    jsonObject = this.participants.getJSONObject(i);
                    if (jsonObject.has("name") && jsonObject.has("color")){
                        names.add(jsonObject.getString("name"));
                        colors.add(jsonObject.getString("color"));
                        HashMap<String, String> hmData = new HashMap<String, String>();
                        hmData.put("id", jsonObject.getString("id"));
                        hmData.put("name", jsonObject.getString("name"));
                        hmData.put("color", jsonObject.getString("color"));
                        entries.put(jsonObject.getString("name"), hmData);
                    }
                } catch (JSONException e) {
                    Log.e("Android", e.getMessage(), e);
                }
            }
        }

        return entries;
    }

    /* CustomSpinnerAdapter */
    public class CustomSpinnerAdapter extends ArrayAdapter {

        public CustomSpinnerAdapter(Context context, int textViewResourceId,
                                    List<SpinnerModel> objects) {
            super(context, textViewResourceId, objects);
        }

        public View getCustomView(int position, View convertView,
                                  ViewGroup parent) {

            LayoutInflater inflater = getLayoutInflater();
            View layout = inflater.inflate(R.layout.custom_spinner, parent, false);

            SpinnerModel spEntry = SpinnerValues.get(position);

            TextView spName = (TextView) layout
                    .findViewById(R.id.sp_name);
            spName.setText(spEntry.getName());

            ImageView spColor = (ImageView) layout.findViewById(R.id.sp_color);
            spColor.setBackgroundColor(Color.parseColor(spEntry.getColor()));

            return layout;
        }

        // It gets a View that displays in the drop down popup the data at the specified position
        @Override
        public View getDropDownView(int position, View convertView,
                                    ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }

        // It gets a View that displays the data at the specified position
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }
    }
}
