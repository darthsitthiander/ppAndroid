package nl.avans.planningpoker.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;

import org.json.JSONObject;

import nl.avans.planningpoker.R;
import nl.avans.planningpoker.models.Singleton;

public class IssueWaitingScreen extends Activity {


    private IssueWaitingScreen _activity;
    private Socket mSocket = Singleton.getInstance(this).mSocket;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d("myapp", "Singleton wordt aangemaakt");
        super.onCreate(savedInstanceState);
        this._activity = this;
        mSocket.on("new issue", onNewIssue);
        mSocket.on("finished", onFinished);
        setContentView(R.layout.activity_issue_waiting_screen);
    }

    @Override
    public void onBackPressed() {
        //Ask the user if they want to quit
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle(R.string.quit)
                .setMessage(R.string.really_quit)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        startActivity(new Intent(_activity, JoinRoomActivity.class));
                        //Stop the activity
                        IssueWaitingScreen.this.finish();
                    }

                })
                .setNegativeButton(R.string.no, null)
                .show();
    }

    private Emitter.Listener onNewIssue = new Emitter.Listener() {

        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    goToIssue(data.toString());
                }
            });
        }
    };

    private Emitter.Listener onFinished = new Emitter.Listener() {

        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    String text = "Het spelen van de sessie is afgelopen";
                    Toast toast = Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT);
                    toast.show();
                }
            });
        }
    };

    private void goToIssue(String data)
    {
        Intent intent = new Intent(this, CardSelectionActivity.class);
        intent.putExtra("json", data);
        startActivity(intent);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        mSocket.off("new issue", onNewIssue);
    }
}
