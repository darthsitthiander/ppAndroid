package nl.avans.planningpoker.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import nl.avans.planningpoker.R;
import nl.avans.planningpoker.models.AsyncResponse;
import nl.avans.planningpoker.models.TaskGET;
import nl.avans.planningpoker.models.TaskPUT;


public class JoinRoomActivity extends Activity {

    private String json;
    private JoinRoomActivity _activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_join_room);
        setTitle(R.string.title_activity_join_room);

        this._activity = this;

        Bundle extras = getIntent().getExtras();

        //used to test to update a field of a issue, getJiraIsues() prints all the jira issues

        if (extras != null) {
            this.json = extras.getString("json");
        } else {
            getJSON();
         }
    }

    private void getJSON()
    {
        TaskGET tJSON = new TaskGET(new AsyncResponse(){
            @Override
            public void processFinish(String output)
            {
                _activity.json = output;
            }
        });
        tJSON.execute("http://p-poker.herokuapp.com/api/sessions/");
    }

    private void GetJiraIssues()
    {
        TaskGET tJSON = new TaskGET(new AsyncResponse(){
            @Override
            public void processFinish(String output)
            {
                Log.d("jira-issues", output);
                _activity.json = output;
            }
        });

        tJSON.addHeader("Authorization", "Basic TWp3c3ByZW46MTIzNEFiY2Q=");

        tJSON.execute("http://145.48.221.29:8080/rest/api/2/search");
    }

    private void updateJiraIssue(String jiraIssue, String field, String content)
    {
        HashMap<String, String> data = new HashMap<String, String>();
        data.put("url", "http://145.48.221.29:8080/rest/api/2/issue/" + jiraIssue);

        JSONObject jobj = new JSONObject();
        JSONObject innerJSON = new JSONObject();

        try {
            innerJSON.put(field, content);
            jobj.put("fields", innerJSON);
        } catch (JSONException e) {
            Log.e("Android", e.getMessage(), e);
        }

        data.put("json", jobj.toString());

        TaskPUT tJSON = new TaskPUT(new AsyncResponse(){
            @Override
            public void processFinish(String output)
            {
                Log.d("finished update", output);
                GetJiraIssues();
            }
        });

        tJSON.addHeader("Authorization", "Basic TWp3c3ByZW46MTIzNEFiY2Q=");
        tJSON.addHeader("Content-Type", "application/json;");

        tJSON.execute(data);
    }


    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_join_room, menu);
        return true;
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent i = new Intent(this, SettingsActivity.class);
            startActivity(i);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onClickBtnEnter(View view){
        TextView txtRoomID = (TextView) findViewById(R.id.txtRoomID);

        String strTxtRoomID = txtRoomID.getText().toString();

        if (!strTxtRoomID.matches("")) {

            if(strTxtRoomID.matches("-?\\d+(\\.\\d+)?")) {
                Integer roomID = Integer.valueOf(strTxtRoomID);

                if(roomID != null) {
                    int key = -1;
                    try {
                        JSONArray jsonA = new JSONArray(json);
                        boolean found = false;
                        for(int i =0; i<jsonA.length(); i++){
                            JSONObject jsonObject = jsonA.getJSONObject(i);

                            if(jsonObject.has("key") && !jsonObject.getString("key").equals("") &&jsonObject.has("id")){
                                String sessionID = jsonObject.getString("id");
                                String sessionKey = jsonObject.getString("key");

                                if(sessionKey.equals(String.valueOf(roomID))) {
                                    saveSessionData(sessionID, sessionKey);
                                    Intent intent = new Intent(this, ExtraInformationActivity.class);
                                    intent.putExtra("session_json", jsonObject.toString());
                                    startActivity(intent);
                                    found = true;
                                    break;
                                }
                            }
                        }
                        if(!found){
                            Toast.makeText(getApplicationContext(), "Could not find room.", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        Log.e("Android", e.getMessage(), e);
                    }

                }
            } else {
                Log.d("Error: ", "Room ID is not a number.");
            }


        } else {
            Log.d("Error: ", "You have not entered a room ID.");

        }

    }

    private void saveSessionData(String sessionID, String sessionKey) {
        SharedPreferences settings = getSharedPreferences("SessionPreferences", 0);
        SharedPreferences.Editor editor = settings.edit();
        Log.d("opslaan sessionid",sessionID);
        editor.putString("sessionID", sessionID);
        editor.putString("sessionKey", sessionKey);

        // Commit the edits!
        editor.commit();
    }
}
