package nl.avans.planningpoker.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;

import org.json.JSONObject;

import java.text.DecimalFormatSymbols;

import nl.avans.planningpoker.R;
import nl.avans.planningpoker.models.Singleton;

public class ShowCardActivity extends Activity {
    private Socket mSocket = Singleton.getInstance().mSocket;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSocket.on("new issue", onNewIssue);
        mSocket.on("finished", onFinished);
        setContentView(R.layout.activity_show_card);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String value = extras.getString("card_value");
            TextView txt_card = (TextView) findViewById(R.id.txt_ws_card);

            // Font Awesome
            Typeface font;
            font = Typeface.createFromAsset(getAssets(), "fontawesome-webfont.ttf");
            txt_card.setTypeface(font);

            String cardValue = "";
            switch(value.toLowerCase()){
                case "infinite": cardValue = DecimalFormatSymbols.getInstance().getInfinity();
                    txt_card.setTypeface(null, Typeface.NORMAL);
                    break; //R.string.fa_spinner //"?"
                case "unsure": cardValue = "?"; break;
                default:cardValue = value;
                    txt_card.setTypeface(null, Typeface.NORMAL);
            }
            txt_card.setText(cardValue);
        }
    }
    private Emitter.Listener onFinished = new Emitter.Listener() {

        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    String text = "Het spelen van de sessie is afgelopen";
                    Toast toast = Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT);
                    toast.show();
                }
            });
        }
    };
    private void goToIssue(String data)
    {
        Intent intent = new Intent(this, CardSelectionActivity.class);
        intent.putExtra("json", data);
        startActivity(intent);
    }
    private Emitter.Listener onNewIssue = new Emitter.Listener() {

        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    goToIssue(data.toString());
                }
            });
        }
    };
    public void backToResults(View view)
    {
        Intent intent = new Intent(this, ResultsActivity.class);
        intent.putExtra("card_value", getIntent().getStringExtra("card_value"));
        intent.putExtra("vote", getIntent().getStringExtra("vote"));
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        //Ask the user if they want to quit
        final ShowCardActivity _activity = this;
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle(R.string.quit)
                .setMessage(R.string.really_quit)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        startActivity(new Intent(_activity, JoinRoomActivity.class));
                        //Stop the activity
                        ShowCardActivity.this.finish();
                    }

                })
                .setNegativeButton(R.string.no, null)
                .show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
