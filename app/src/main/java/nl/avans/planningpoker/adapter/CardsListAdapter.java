package nl.avans.planningpoker.adapter;

import android.app.Activity;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.text.DecimalFormatSymbols;
import java.util.List;

import nl.avans.planningpoker.R;
import nl.avans.planningpoker.models.Card;

/**
 * Created by Sander on 12-3-2015.
 */
public class CardsListAdapter extends ArrayAdapter<Card> {
    private LayoutInflater inflater;

    public CardsListAdapter(Activity activity, List<Card> items) {
        super(activity, R.layout.l_cards_item_row, items);
        inflater = activity.getWindow().getLayoutInflater();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        View view = convertView;
        if( convertView == null ){
            //We must create a View:
            view = inflater.inflate(R.layout.l_cards_item_row, parent, false);
        }
        TextView header = (TextView) view.findViewById(R.id.textView1);
        String cardName = getItem(position).getName();
        Typeface font = Typeface.createFromAsset( getContext().getAssets(), "fontawesome-webfont.ttf" );
        header.setTypeface(font);
        switch(cardName.toLowerCase()){
            case "coffee": header.setText(R.string.fa_coffee); break;
            case "infinite": header.setText(DecimalFormatSymbols.getInstance().getInfinity());
                header.setTypeface(null, Typeface.NORMAL);
                break; //R.string.fa_spinner //"?"
            case "unsure": header.setText(R.string.fa_question); break;
            default: header.setText(cardName);
                header.setTypeface(null, Typeface.NORMAL);
        }
        //Here we can do changes to the convertView, such as set a text on a TextView
        //or an image on an ImageView.
        return view;
    }

}