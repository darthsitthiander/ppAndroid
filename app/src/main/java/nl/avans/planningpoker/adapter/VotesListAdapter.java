package nl.avans.planningpoker.adapter;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.text.DecimalFormatSymbols;
import java.util.List;

import nl.avans.planningpoker.R;
import nl.avans.planningpoker.models.Vote;

/**
 * Created by Sander on 12-3-2015.
 */
public class VotesListAdapter extends ArrayAdapter<Vote> {
    private LayoutInflater inflater;

    public VotesListAdapter(Activity activity, List<Vote> items) {
        super(activity, R.layout.l_results_item_row, items);
        inflater = activity.getWindow().getLayoutInflater();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        View view = convertView;
        if( convertView == null ){
            //We must create a View:
            view = inflater.inflate(R.layout.l_results_item_row, parent, false);
        }
        TextView txt_name = (TextView) view.findViewById(R.id.txt_list_results_name);
        txt_name.setText(getItem(position).getUser());
        txt_name.setTypeface(null, Typeface.NORMAL);

        String color = getItem(position).getColor();

        ImageView viewcolor = (ImageView)view.findViewById(R.id.imageView1);
        viewcolor.setBackgroundColor(Color.parseColor(color));

        String vote = getItem(position).getVote();
        if("unsure".equalsIgnoreCase(vote)){
            vote = "?";
        } else if ("infinite".equalsIgnoreCase(vote)){
            vote = DecimalFormatSymbols.getInstance().getInfinity();
        }
        TextView txt_vote = (TextView) view.findViewById(R.id.txt_list_results_vote);
        txt_vote.setText(vote);
        txt_vote.setTypeface(null, Typeface.NORMAL);


        return view;
    }

}